/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     16/11/2021 3:32:48                           */
/*==============================================================*/


/*==============================================================*/
/* Table: CLIENTE                                               */
/*==============================================================*/
create table CLIENTE (
   CLIENTE_ID           INT4                 not null,
   LUGAR_ID             INT4                 not null,
   VENTAS_ID            INT4                 not null,
   NOMBRES_CLIENTE      TEXT                 null,
   APELLIDOS_CLIENTE    TEXT                 null,
   CEDULA_CLIENTE       CHAR(10)             null,
   NUM_TELEFONO_CLIENTE CHAR(10)             null,
   CORREO_CLIENTE       TEXT                 null,
   GENERO_CLIENTE       TEXT                 null,
   DIRECCION_CLIENTE    TEXT                 null,
   constraint PK_CLIENTE primary key (CLIENTE_ID)
);

/*==============================================================*/
/* Index: CLIENTE_PK                                            */
/*==============================================================*/
create unique index CLIENTE_PK on CLIENTE (
CLIENTE_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_1_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_1_FK on CLIENTE (
LUGAR_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_8_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_8_FK on CLIENTE (
VENTAS_ID
);

/*==============================================================*/
/* Table: COMPRA                                                */
/*==============================================================*/
create table COMPRA (
   COMPRAS_ID           INT4                 not null,
   TOTAL_COMPRA         NUMERIC              null,
   FECHA_COMPRA         DATE                 null,
   constraint PK_COMPRA primary key (COMPRAS_ID)
);

/*==============================================================*/
/* Index: COMPRA_PK                                             */
/*==============================================================*/
create unique index COMPRA_PK on COMPRA (
COMPRAS_ID
);

/*==============================================================*/
/* Table: LINE_COMPRA                                           */
/*==============================================================*/
create table LINE_COMPRA (
   COMPRAS_ID           INT4                 not null,
   PROVEEDOR_ID         INT4                 not null,
   constraint PK_LINE_COMPRA primary key (COMPRAS_ID, PROVEEDOR_ID)
);

/*==============================================================*/
/* Index: RELATIONSHIP_10_PK                                    */
/*==============================================================*/
create unique index RELATIONSHIP_10_PK on LINE_COMPRA (
COMPRAS_ID,
PROVEEDOR_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_13_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_13_FK on LINE_COMPRA (
PROVEEDOR_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_10_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_10_FK on LINE_COMPRA (
COMPRAS_ID
);

/*==============================================================*/
/* Table: LUGAR                                                 */
/*==============================================================*/
create table LUGAR (
   LUGAR_ID             INT4                 not null,
   PAIS_CLIENTE         TEXT                 null,
   CIUDAD_CLIENTE       TEXT                 null,
   CANTON_CLIENTE       TEXT                 null,
   constraint PK_LUGAR primary key (LUGAR_ID)
);

/*==============================================================*/
/* Index: LUGAR_PK                                              */
/*==============================================================*/
create unique index LUGAR_PK on LUGAR (
LUGAR_ID
);

/*==============================================================*/
/* Table: MANTENIMIENTO                                         */
/*==============================================================*/
create table MANTENIMIENTO (
   MANTENIMIENTO_ID     INT4                 not null,
   TECNICOS_ID          INT4                 not null,
   CLIENTE_ID           INT4                 not null,
   TIPO_ID              INT4                 not null,
   GARANTIA_MANTENIMIENTO TEXT                 null,
   FECHA_RECEPCION      DATE                 null,
   FECHA_ENTREGA        DATE                 null,
   COSTO_MANTENIMIENTO  NUMERIC              null,
   ESTADO_MANTENIMIENTO TEXT                 null,
   constraint PK_MANTENIMIENTO primary key (MANTENIMIENTO_ID)
);

/*==============================================================*/
/* Index: MANTENIMIENTO_PK                                      */
/*==============================================================*/
create unique index MANTENIMIENTO_PK on MANTENIMIENTO (
MANTENIMIENTO_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_5_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_5_FK on MANTENIMIENTO (
TECNICOS_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_7_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_7_FK on MANTENIMIENTO (
CLIENTE_ID
);

/*==============================================================*/
/* Table: PRODUCTO                                              */
/*==============================================================*/
create table PRODUCTO (
   PRODUCTOS_ID         INT4                 not null,
   PROVEEDOR_ID         INT4                 not null,
   MANTENIMIENTO_ID     INT4                 not null,
   NOMBRE_PRODUCTO      TEXT                 null,
   NUM_SERIE_PRODUCTO   NUMERIC              null,
   FECHA_PRODUCTO       DATE                 null,
   constraint PK_PRODUCTO primary key (PRODUCTOS_ID)
);

/*==============================================================*/
/* Index: PRODUCTO_PK                                           */
/*==============================================================*/
create unique index PRODUCTO_PK on PRODUCTO (
PRODUCTOS_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_2_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_2_FK on PRODUCTO (
PROVEEDOR_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_3_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_3_FK on PRODUCTO (
MANTENIMIENTO_ID
);

/*==============================================================*/
/* Table: PROVEEDOR                                             */
/*==============================================================*/
create table PROVEEDOR (
   PROVEEDOR_ID         INT4                 not null,
   NOMBRES_PROVEEDOR    TEXT                 null,
   APELLIDOS_PROVEEDOR  TEXT                 null,
   CEDULA_PROVEEDOR     CHAR(10)             null,
   NUM_TELEFONO_PRO     CHAR(10)             null,
   CORREO_PROVEEDOR     TEXT                 null,
   SITIO_WEB_PROVEEDOR  TEXT                 null,
   constraint PK_PROVEEDOR primary key (PROVEEDOR_ID)
);

/*==============================================================*/
/* Index: PROVEEDOR_PK                                          */
/*==============================================================*/
create unique index PROVEEDOR_PK on PROVEEDOR (
PROVEEDOR_ID
);

/*==============================================================*/
/* Table: RELATIONSHIP_4                                        */
/*==============================================================*/
create table RELATIONSHIP_4 (
   VENTAS_ID            INT4                 not null,
   PRODUCTOS_ID         INT4                 not null,
   constraint PK_RELATIONSHIP_4 primary key (VENTAS_ID, PRODUCTOS_ID)
);

/*==============================================================*/
/* Index: RELATIONSHIP_4_PK                                     */
/*==============================================================*/
create unique index RELATIONSHIP_4_PK on RELATIONSHIP_4 (
VENTAS_ID,
PRODUCTOS_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_12_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_12_FK on RELATIONSHIP_4 (
PRODUCTOS_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_4_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_4_FK on RELATIONSHIP_4 (
VENTAS_ID
);

/*==============================================================*/
/* Table: TECNICO                                               */
/*==============================================================*/
create table TECNICO (
   TECNICOS_ID          INT4                 not null,
   TIEMPO_ID            INT4                 not null,
   NOMBRES_TECNICO      TEXT                 null,
   APELLIDOS_TECNICO    TEXT                 null,
   CEDULA_TECNICOS      CHAR(10)             null,
   NUM_TELEFONO_TECNICO CHAR(10)             null,
   constraint PK_TECNICO primary key (TECNICOS_ID)
);

/*==============================================================*/
/* Index: TECNICO_PK                                            */
/*==============================================================*/
create unique index TECNICO_PK on TECNICO (
TECNICOS_ID
);

/*==============================================================*/
/* Table: TIEMPO_LABORABLE                                      */
/*==============================================================*/
create table TIEMPO_LABORABLE (
   TIEMPO_ID            INT4                 not null,
   HORAS_TRABAJO        TIME                 null,
   constraint PK_TIEMPO_LABORABLE primary key (TIEMPO_ID)
);

/*==============================================================*/
/* Index: TIEMPO_LABORABLE_PK                                   */
/*==============================================================*/
create unique index TIEMPO_LABORABLE_PK on TIEMPO_LABORABLE (
TIEMPO_ID
);

/*==============================================================*/
/* Table: TIPO_MANTENIMIENTO                                    */
/*==============================================================*/
create table TIPO_MANTENIMIENTO (
   TIPO_ID              INT4                 not null,
   TIPO_MANTENIMIENTO   TEXT                 null,
   constraint PK_TIPO_MANTENIMIENTO primary key (TIPO_ID)
);

/*==============================================================*/
/* Index: TIPO_MANTENIMIENTO_PK                                 */
/*==============================================================*/
create unique index TIPO_MANTENIMIENTO_PK on TIPO_MANTENIMIENTO (
TIPO_ID
);

/*==============================================================*/
/* Table: TIPO_PRODUCTO                                         */
/*==============================================================*/
create table TIPO_PRODUCTO (
   TIPOPRODUC_ID        INT4                 not null,
   VENTAS_ID            INT4                 not null,
   TIPO_PRODUCTO        TEXT                 null,
   constraint PK_TIPO_PRODUCTO primary key (TIPOPRODUC_ID)
);

/*==============================================================*/
/* Index: TIPO_PRODUCTO_PK                                      */
/*==============================================================*/
create unique index TIPO_PRODUCTO_PK on TIPO_PRODUCTO (
TIPOPRODUC_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_11_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_11_FK on TIPO_PRODUCTO (
VENTAS_ID
);

/*==============================================================*/
/* Table: VENTA                                                 */
/*==============================================================*/
create table VENTA (
   VENTAS_ID            INT4                 not null,
   GARANTIA_VENTA       TEXT                 null,
   TOTAL_VENTA          NUMERIC              null,
   constraint PK_VENTA primary key (VENTAS_ID)
);

/*==============================================================*/
/* Index: VENTA_PK                                              */
/*==============================================================*/
create unique index VENTA_PK on VENTA (
VENTAS_ID
);

alter table CLIENTE
   add constraint FK_CLIENTE_RELATIONS_LUGAR foreign key (LUGAR_ID)
      references LUGAR (LUGAR_ID)
      on delete restrict on update restrict;

alter table CLIENTE
   add constraint FK_CLIENTE_RELATIONS_VENTA foreign key (VENTAS_ID)
      references VENTA (VENTAS_ID)
      on delete restrict on update restrict;

alter table LINE_COMPRA
   add constraint FK_LINE_COM_RELATIONS_COMPRA foreign key (COMPRAS_ID)
      references COMPRA (COMPRAS_ID)
      on delete restrict on update restrict;

alter table LINE_COMPRA
   add constraint FK_LINE_COM_RELATIONS_PROVEEDO foreign key (PROVEEDOR_ID)
      references PROVEEDOR (PROVEEDOR_ID)
      on delete restrict on update restrict;

alter table MANTENIMIENTO
   add constraint FK_MANTENIM_RELATIONS_TECNICO foreign key (TECNICOS_ID)
      references TECNICO (TECNICOS_ID)
      on delete restrict on update restrict;

alter table MANTENIMIENTO
   add constraint FK_MANTENIM_RELATIONS_CLIENTE foreign key (CLIENTE_ID)
      references CLIENTE (CLIENTE_ID)
      on delete restrict on update restrict;

alter table MANTENIMIENTO
   add constraint FK_MANTENIM_RELATIONS_TIPO_MAN foreign key (TIPO_ID)
      references TIPO_MANTENIMIENTO (TIPO_ID)
      on delete restrict on update restrict;

alter table PRODUCTO
   add constraint FK_PRODUCTO_RELATIONS_PROVEEDO foreign key (PROVEEDOR_ID)
      references PROVEEDOR (PROVEEDOR_ID)
      on delete restrict on update restrict;

alter table PRODUCTO
   add constraint FK_PRODUCTO_RELATIONS_MANTENIM foreign key (MANTENIMIENTO_ID)
      references MANTENIMIENTO (MANTENIMIENTO_ID)
      on delete restrict on update restrict;

alter table RELATIONSHIP_4
   add constraint FK_RELATION_RELATIONS_PRODUCTO foreign key (PRODUCTOS_ID)
      references PRODUCTO (PRODUCTOS_ID)
      on delete restrict on update restrict;

alter table RELATIONSHIP_4
   add constraint FK_RELATION_RELATIONS_VENTA foreign key (VENTAS_ID)
      references VENTA (VENTAS_ID)
      on delete restrict on update restrict;

alter table TECNICO
   add constraint FK_TECNICO_RELATIONS_TIEMPO_L foreign key (TIEMPO_ID)
      references TIEMPO_LABORABLE (TIEMPO_ID)
      on delete restrict on update restrict;

alter table TIPO_PRODUCTO
   add constraint FK_TIPO_PRO_RELATIONS_VENTA foreign key (VENTAS_ID)
      references VENTA (VENTAS_ID)
      on delete restrict on update restrict;

